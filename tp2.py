class Box:
    def __init__(self, opened=False,capacity=None):
        self._contents = []
        self._opened = opened
        self._capacity = capacity

    def __contains__(self, machin):
        return machin in self.getContents()

    def add(self, truc):
        self._contents.append(truc)
    
    def getContents(self):
        return self._contents

    def remove(self, truc):
        self._contents.remove(truc)

    def open(self):
        self._opened = True

    def close(self):
        self._opened = False

    def is_open(self):
        return self._opened

    def action_look(self):
        if self.is_open():
            return "la boite contient: " + ", ".join(self.getContents())
        else:
            return "la boite est fermee"

    def set_capacity(self, capacity):
        self._capacity = capacity

    def capacity(self):
        return self._capacity

    def has_room_for(self, t):
        return self.capacity() == None or t.volume() <= self.capacity()

    def action_add(self, t):
        if self.has_room_for(t) and self.is_open():
            self.add(t)
            if(self.capacity() != None):
                self.set_capacity(self.capacity() - t.volume())
            return True
        else:
            return False

    def find(self, name):
        if not self.is_open():
            return None
        for thing in self.getContents():
            if thing.has_name(name):
                return thing
        return None


class Thing:
    def __init__(self, volume, name=None):
        self._volume = volume
        self._name = name

    def volume(self):
        return self._volume
    
    def set_name(self, name):
        self._name = name

    def has_name(self, name):
        return self._name == name

    def __repr__(self):
        return self._name