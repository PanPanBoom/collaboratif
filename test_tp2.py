from tp2 import *

# on veut pouvoir créer des boites
def test_box_create():
    b = Box()

def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")

def test_box_contains():
    b = Box()
    b.add("truc1")
    assert "truc1" in b
    assert "truc2" not in b

def test_box_remove():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.remove("truc1")
    assert "truc1" not in b
    assert "truc2" in b

def test_box_open_and_close():
    b = Box()
    assert not b.is_open()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()

def test_box_action_look():
    b = Box()
    b.add("ceci")
    b.add("cela")
    b.open()
    assert b.action_look() == "la boite contient: ceci, cela"
    b.close()
    assert b.action_look() == "la boite est fermee"

def test_box_capacity():
    b = Box()
    b.set_capacity(5)
    assert b.capacity() == 5

def test_box_capacity_None():
    b = Box()
    assert b.capacity() == None

def test_box_has_room_for():
    b = Box()
    t = Thing(3)
    t2 = Thing(6)
    t3 = Thing(5)
    t4 = Thing(10000)
    assert b.has_room_for(t4)
    b.set_capacity(5)
    assert b.has_room_for(t)
    assert not b.has_room_for(t2)
    assert b.has_room_for(t3)
    
def test_box_action_add():
    b = Box()
    t = Thing(3)
    t2 = Thing(10)
    t3 = Thing(7)
    t4 = Thing(1)
    b.set_capacity(10)
    assert not b.action_add(t)
    b.open()
    assert b.action_add(t)
    assert not b.action_add(t2)
    assert b.action_add(t3)
    assert not b.action_add(t4)

def test_box_find():
    b = Box()
    t = Thing(2)
    t.set_name("truc")
    b.open()
    b.action_add(t)
    assert b.find("truc") == t
    assert b.find("machin") == None
    b.close()
    assert b.find("truc") == None

def test_box_construct_open_capacity():
    b = Box()
    b = Box(True)
    b = Box(True, 5)

def test_thing_create():
    t = Thing(3)

def test_thing_volume():
    t = Thing(3)
    assert t.volume() == 3

def test_thing_set_name():
    t = Thing(3)
    t.set_name("bidule")

def test_thing_has_name():
    t = Thing(3)
    t.set_name("bidule")
    assert t.has_name("bidule")
    assert not t.has_name("truc")

def test_thing_construct_name():
    t = Thing(3)
    t = Thing(3, "truc")

def test_bidon():
    assert True

def test_bidon2():
    assert True

def unAutreTestBidon():
    assert True