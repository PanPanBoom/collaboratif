import yaml

with open("contact.yaml", "r") as flux:
    print(yaml.load(flux))

class Person:
    def __init__(self, prenom=None, nom=None, adresse=None):
        self.prenom = prenom
        self.nom = nom
        self.adresse = adresse

    @staticmethod
    def from_file(nomfic):
        res = []
        with open(nomfic, "r") as flux:
            personnes = yaml.load(flux)
        for p in personnes:
            nom = p.get("nom")
            prenom = p.get("prenom")
            adresse = p["adresse"]
            res.append(Person(prenom, nom, adresse))
        return res

    def __repr__(self):
        return "\nPersonne :\n %s,%s, %s\n\n" %(self.prenom, self.nom, self.adresse)


lp = Person.from_file("contact.yaml")
for p in lp:
    print(p)